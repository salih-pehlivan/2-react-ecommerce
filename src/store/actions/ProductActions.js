import axios from 'axios';

// Tum urunleri getirir:
export const getProducts = () => dispatch => {
  axios
    .get('http://localhost:4000/products')
    .then(response => {
      dispatch({ type: 'GET_PRODUCTS', payload: [...response.data] })
    })
    .catch(error => dispatch({ type: 'GET_PRODUCTS', payload: error }))
}

// Urunu sepete ekler:
export const addToCart = (payload) => dispatch => {
  const addedProduct = { ...payload, piece: 1 }
  console.log('add cart');
  axios
  .post('http://localhost:4000/cart', addedProduct)
  .then(response => {
    dispatch({ type: 'ADD_CART', payload: response.data })
  })
  .catch(error => dispatch({ type: 'ADD_CART', payload: error }))
}

// Sepet icerigini getirir:
export const getCart = () => dispatch => {
  axios
    .get('http://localhost:4000/cart')
    .then(response => {
      dispatch({ type: 'SET_CART', payload: response.data })
    })
    .catch(error => dispatch({}))
}

// Sepetten bir urun siler:
export const removeCartItem = (payload) => dispatch => {
  axios
    .delete(`http://localhost:4000/cart/${payload}`)
    .then(response => {
      dispatch({ type: 'REMOVE_CART_ITEM', payload: payload })
    })
    .catch(error => dispatch({}))
}

// Sepetteki bir urunun adedini degistirir:
export const changePiece = (payload) => dispatch => {
  axios
    .put(`http://localhost:4000/cart/${payload.id}`, payload)
    .then(response => {
      dispatch({ type: 'CHANGE_PIECE', payload: response.data })
    })
    .catch(error => dispatch({}))
}
