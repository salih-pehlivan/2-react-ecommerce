import axios from 'axios';

export const setTheme = (payload) => {
  return { type: 'SET_THEME', payload }
}

export const getGeneral = () => dispatch => {
  axios.get('http://localhost:4000/general').then(response => {
    console.log('Response', response.data);
    dispatch({ type: 'SET_GENERAL', payload: response.data })
  }).catch(error => console.log('ERROR'))
}