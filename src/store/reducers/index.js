import { ProductReducers } from './ProductReducers'
import { ThemeReducers } from './ThemeReducers'
import { combineReducers } from 'redux'
export const reducer = combineReducers({
  productReducers: ProductReducers,
  themeReducers: ThemeReducers
})