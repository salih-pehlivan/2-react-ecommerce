const initialState = {
  products: [],
  cart: []
}

export const ProductReducers = (state = initialState, action) => {
  switch(action.type) {
    case 'GET_PRODUCTS' :
      return { ...state, products: action.payload }
    case 'ADD_CART' :
      return {...state, cart: [...state.cart, action.payload]}
    // Db'den gelen cart bilgilerini state'ye yazar:
    case 'SET_CART' :
      return { ...state, cart: [...action.payload]}
    case 'REMOVE_CART_ITEM' :
      return { ...state, cart: [...state.cart.filter(f => f.id !== action.payload) ]}
    case 'CHANGE_PIECE' :
      return { ...state, cart: [...state.cart.map(m => m.id === action.payload.id ? action.payload : m)]}
    default :
      return state
  }
}