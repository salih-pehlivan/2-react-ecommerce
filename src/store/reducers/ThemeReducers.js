const initialState = {
  general: {
    name: '',
    description: '',
    version: '',
    currency: '',
    theme: 'light'
  }
}

export const ThemeReducers = (state = initialState, action) => {
  switch(action.type) {
    case 'SET_THEME' :
      return { ...state, general: { ...state.general, theme: action.payload } }
    case 'SET_GENERAL' :
      return { ...state, general: { ...action.payload } }
    default :
      return state
  }
}