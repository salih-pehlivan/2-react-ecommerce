import LeftBlock from './components/LeftBlock/LeftBlock';

const Layout = (props) => {
  return (
    <>
      <div className="left-block">
        <LeftBlock />
      </div>
      <div className="content">
        {props.children}
      </div>
    </>
  )
}

export default Layout
