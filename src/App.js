import './App.scss';

import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getGeneral } from './store/actions/ThemeActions';

// Components:
import Header from './components/Header/Header';
import { BrowserRouter as Router, Route } from 'react-router-dom';

// Layout:
import Layout from './Layout';

// Pages:
import Home from './pages/Home/Home';
import About from './pages/About/About';
import List from './pages/List/List';
import Product from './pages/List/Product/Product';

const App = () => {
  const dispatch = useDispatch()
  const theme = useSelector(state => state.themeReducers.general.theme)
  useEffect(() => {
    // site bilgilerini getir:
    dispatch(getGeneral())
  }, [dispatch])
  
  return (
    <Router>
      <div className={`App ${theme}`}>
        <Header />
        <div className="main">
          <Route exact path="/">
           <Home />
          </Route>
          <Route path="/about">
            <Layout>
              <About />
            </Layout>
          </Route>
          <Route exact path="/list">
            <Layout>
              <List />
            </Layout>
          </Route>
          <Route path="/list/:id">
            <Layout>
              <Product />
            </Layout>
          </Route>
        </div>
      </div>
    </Router>
  );
}

export default App;
