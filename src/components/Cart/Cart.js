import "./Cart.scss";
import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  getCart,
  removeCartItem,
  changePiece
} from "../../store/actions/ProductActions";

const Cart = () => {
  const [cartBottomStatus, setCartBottomStatus] = useState(false);
  const [total, setTotal] = useState(0);
  const dispatch = useDispatch();
  const cart = useSelector(state => state.productReducers.cart, []) || [];
  const currency = useSelector(state => state.themeReducers.general.currency);

  useEffect(() => {
    dispatch(getCart());
  }, [dispatch]);

  useEffect(() => {
    const totalPrice = cart.reduce((accumulator, item) => {
    return accumulator + (item.price * item.piece);
  }, 0);
  setTotal(totalPrice);
  }, [cart]);

  const deleteCartItem = (item) => {
    dispatch(removeCartItem(item.id));
    // silinen urunun fiyatini toplamdan duselim:
    // const total = cart.total - (item.piece * item.price);
    // setTotalPrice(total);
  };

  const changePieceOnCart = (op, id) => {
    cart.map((item) => {
      if (item.id === id) {
        if (op === "plus") {
          item.piece += 1;
          // const total = cart.total + item.price;
          // setTotalPrice(total);
        }
        if (op === "minus") {
          if (item.piece === 1) return false;
          item.piece -= 1;
          // const total = cart.total - item.price;
          // setTotalPrice(total);
        }
        dispatch(changePiece(item));
      }
      return item;
    });
  };

  return (
    <div className={`cart ${cartBottomStatus ? "active" : ""}`}>
      <div
        className="cart-top"
        onClick={() => {
          setCartBottomStatus(!cartBottomStatus);
        }}
      >
        <h4>
          Sepet <span>({cart.length})</span>
        </h4>
      </div>
      <div className="cart-bottom">
        {cart.length ? (
          <div className="cart-total-inside">
            <div className="cart-items">
              {cart.map((item, index) => (
                <div className="cart-item" key={index}>
                  <div className="image">
                    <img src={`/images/${item.imagePath}`} alt={item.name} />
                  </div>
                  <div className="content">
                    <div className="name">{item.name}</div>
                    <div className="price">
                      <b>
                        {item.price} {item.currency}
                      </b>
                    </div>
                    <div className="piece">{item.piece} Adet</div>
                    <div className="change-piece">
                      <div
                        className="plus"
                        onClick={() => changePieceOnCart("plus", item.id)}
                      >
                        +
                      </div>
                      <div
                        className="minus"
                        onClick={() => changePieceOnCart("minus", item.id)}
                      >
                        -
                      </div>
                    </div>
                  </div>
                  <div
                    className="delete"
                    onClick={() => deleteCartItem(item)}
                  ></div>
                </div>
              ))}
            </div>
            <div className="cart-total">Toplam: {total} {currency}</div>
          </div>
        ) : (
          <div className="cart-empty">Sepette hiç ürün bulunmuyor.</div>
        )}
      </div>
    </div>
  );
};

export default Cart;
