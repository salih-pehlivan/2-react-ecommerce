import './LeftBlock.scss'
import { useSelector, useDispatch } from 'react-redux'
import { setTheme } from '../../store/actions/ThemeActions'

const LeftBlock = () => {
  const themeOnState = useSelector(state => state.themeReducers.theme)
  const dispatch = useDispatch();
  
  function changeTheme(theme) {
    if (theme !== themeOnState) {
      dispatch(setTheme(theme))
    }
  }

  return (
    <div className="left-block-inside">
      <div>Left block...</div>
      <div className="change-theme">
        <span>Theme: </span>
        <span className="change-theme-item" onClick={() => changeTheme('dark')}>Dark</span>/
        <span className="change-theme-item" onClick={() => changeTheme('light')}>Light</span>
      </div>
    </div>
  )
}

export default LeftBlock
