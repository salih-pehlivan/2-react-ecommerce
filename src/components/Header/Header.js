import './Header.scss'
import { NavLink } from "react-router-dom"
import Cart from '../Cart/Cart'

const Header = () => {
  return (
    <header className="header">
      <ul className="main-menu">
          <li>
            <NavLink to="/" exact activeClassName="active">Home</NavLink>
          </li>
          <li>
            <NavLink to="/about" activeClassName="active">About</NavLink>
          </li>
          <li>
            <NavLink to="/list" activeClassName="active">Products</NavLink>
          </li>
        </ul>
        <Cart />
    </header>
  )
}

export default Header
