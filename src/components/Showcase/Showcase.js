import './Showcase.scss';
import { useDispatch, useSelector } from 'react-redux'
import { addToCart, changePiece } from '../../store/actions/ProductActions'
import { Link } from "react-router-dom"

const Showcase = ({ product }) => {
  // const total = useSelector((state) => state.productReducers.cart.total, 0) || 0;
  const dispatch = useDispatch()
  const cartItems = useSelector(state => state.productReducers.cart)
  const addItem = (e, product) => {
    e.preventDefault()
    const productInCart = cartItems.filter(cartItem => cartItem.id === product.id)
    if (productInCart.length) { // Sepette ekli ise adedini artir
      productInCart[0]['piece'] += 1;
      dispatch(changePiece(productInCart[0]))
    } else { // Sepette ekli degilse ekle
      dispatch(addToCart(product))
    }
    // const newTotal = total + product.price;
    // setTotalPrice(newTotal);
  }

  return (
    <div className="showcase">
      <div className="showcase-image">
        <img src={`/images/${product.imagePath}`} alt={product.name} />
      </div>
      <div className="showcase-info">
        <div className="showcase-name">
          {product.name}
        </div>
        <div className="showcase-price">
          {product.price} {product.currency}
        </div>
        <div className="showcase-buttons">
          <a
            href="!#"
            onClick={(e) => addItem(e, product)}>
            Sepete Ekle
          </a>
          <Link to={`/list/${product.id}`}>Incele Beni</Link>
        </div>
      </div>
    </div>
  )
}

export default Showcase
