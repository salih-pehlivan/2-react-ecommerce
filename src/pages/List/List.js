import "./List.scss";
import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getProducts } from "../../store/actions/ProductActions";

import Showcase from "../../components/Showcase/Showcase";

const List = () => {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.productReducers.products, []) || [];
  useEffect(() => {
    dispatch(getProducts());
  }, [dispatch]);

  return (
    <div className="page-list">
      <div className="page-list-head">
        <h3 className="page-title">Products</h3>
      </div>
      <div className="products">
        {products.map((product) => {
          return <Showcase key={product.id} product={product} />;
        })}
      </div>
    </div>
  );
};

export default List;
