import './Product.scss'
import { useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addToCart, changePiece } from '../../../store/actions/ProductActions'
import axios from 'axios';

const Product = () => {
  const { id } = useParams();
  const [product, setProduct] = useState({});
  useEffect(() => {
    axios
    .get(`http://localhost:4000/products/${id}`)
    .then(response => {
      setProduct(response.data)
    })
  }, [id])
  const dispatch = useDispatch()
  const cartItems = useSelector(state => state.productReducers.cart.items)
  const addItem = (e, product) => {
    e.preventDefault()
    const productInCart = cartItems.filter(cartItem => cartItem.id === product.id)
    if (productInCart.length) { // Sepette ekli ise adedini artir
      productInCart[0]['piece'] += 1;
      dispatch(changePiece(productInCart[0]))
    } else { // Sepette ekli degilse ekle
      dispatch(addToCart(product))
    }
  }
  return (
    <div>
      <h1>{product.name}</h1>
      <hr />
      <img src={`/images/${product.imagePath}`} alt={product.name} />
      <div className="product-price"><b>Fiyat: </b>{product.price} {product.currency}</div>
      <a
        href="!#"
        className="add-cart"
        onClick={(e) => addItem(e, product)}>
        Sepete Ekle
      </a>
    </div>
  )
}

export default Product
